public class Dinosaur extends Animal {

   int mHeight;

    public Dinosaur(String animalName, boolean vegetarian, int numberOfLegs, double weight, int timeToBeHungry, int mHeight) {
        super(animalName, vegetarian, numberOfLegs, weight, timeToBeHungry);
        this.mHeight = mHeight;
    }
}

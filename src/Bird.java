public class Bird extends Animal {
    double flightSpeed;
    int wingsWidth;
    String wingsColor;

    public Bird(String animalName, boolean vegetarian, int numberOfLegs, double weight, int timeToBeHungry, double flightSpeed, int wingsWidth, String wingsColor) {
        super(animalName, vegetarian, numberOfLegs, weight, timeToBeHungry);
        this.flightSpeed = flightSpeed;
        this.wingsWidth = wingsWidth;
        this.wingsColor = wingsColor;
    }

    @Override
    public boolean canFly(boolean fly) {
        return fly;
    }
}

public class Dragon extends Animal {

    private int numberOfHeads;
    private int FireDistance;

    public Dragon(String animalName, Boolean vegetrian, int numberOfLegs, double weight, int timeToBeHungry, int numberOfHeads, int fireDistance) {
        super(animalName, vegetrian, numberOfLegs, weight, timeToBeHungry);
        this.numberOfHeads = numberOfHeads;
        FireDistance = fireDistance;
    }

    public int getNumberOfHeads()

    {
        return numberOfHeads;
    }


    public int getFireDistance()

    {
        return FireDistance;
    }


    @Override
    public boolean canFly(boolean fly) {
        return false;
    }
}

/**
 * Created by maayantener on 06/02/2018.
 */
public class Carebear extends Animal {

    private String signal;

    public Carebear(String animalName, Boolean vegetrian, int numberOfLegs, double weight, int timeToBeHungry, String signal) {
        super(animalName, vegetrian, numberOfLegs, weight, timeToBeHungry);
        this.signal = signal;
    }
}

public abstract class Animal {
    String animalName;
    boolean vegetarian;
    int numberOfLegs;
    double weight;
    int timeToBeHungryInMin;
    long lastFedTime;

    public Animal(String animalName, boolean vegetarian, int numberOfLegs, double weight, int timeToBeHungryInMin) {
        this.animalName = animalName;
        this.vegetarian = vegetarian;
        this.numberOfLegs = numberOfLegs;
        this.weight = weight;
        this.timeToBeHungryInMin = timeToBeHungryInMin;
    }
    public long getLastFedTime() {
        return lastFedTime;
    }

    public void setLastFedTime(long lastFedTime) {
        this.lastFedTime = lastFedTime;
    }

    void feed(){
        lastFedTime=System.currentTimeMillis();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Animal)) {
            return false;
        } else {
            Animal other = (Animal) obj;
            return other.animalName.equals(animalName) && other.vegetarian == vegetarian && other.numberOfLegs == numberOfLegs;
        }
    }

    public void setNumberOfLegs(int numberOfLegs) {
        if (numberOfLegs <= 0){
            throw new IllegalArgumentException("number of legs can not be zero or less.");
        }
        this.numberOfLegs = numberOfLegs;
    }

    public boolean isPetable() {
        return this instanceof Petable;
    }
    public abstract boolean canFly(boolean fly);

    public boolean isHungry() {
        long timeToBeHungryInMillis = timeToBeHungryInMin * 60 * 1000;
        if (timeToBeHungryInMillis < (System.currentTimeMillis() - lastFedTime)){
            return true;
        }
        return false;
    }

    public boolean isDangerous(){
        if (weight>200 && isHungry()==true){
            System.out.println("Dangerous");
            return true;
        }
        return false;

    }
}
import java.util.ArrayList;

public class Zoo {

    ArrayList<Animal> animals;

    public Zoo() {
        animals = new ArrayList<>();
    }


    public boolean addNewAnimal(Animal animal) {

        if (animals.contains(animal)) {
            return false;
        }

        animals.add(animal);
        return true;
    }
}